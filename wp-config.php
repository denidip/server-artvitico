<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'artvitico');

/** Имя пользователя MySQL */
define('DB_USER', 'artvitico');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'XR3L8DBT4fayQKzU');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<{[6KGM H.27^<a@w1I!<9,bh2cXyweectifyUsCz5l-*D&p+ACZKxY{)=<;QDPs');
define('SECURE_AUTH_KEY',  'pYTn!-0imLc&8*{P|kqWJ>kZ@8=}*WhT*jf<)h;~~C B,ggTSeJ}.2XLuc(l{~dz');
define('LOGGED_IN_KEY',    ':{PaCL>(.E{0I/O2vN^Z+pa{C9_G/wup*.SZ65B*.<s4<o4_~xY}=? `6XkauP7 ');
define('NONCE_KEY',        'B,d Lb ?o eAj^DJdi:hWVH_ -W/YTPB17TS8/=)(|!;[zz29#>{i},ro*(&#3O=');
define('AUTH_SALT',        'wH63~{`I,c,2F_AY(f-Ly@7WZF@HLz]$kE-BK4OU%#ZYA/x:e.|-_B/)Lm&AhZPn');
define('SECURE_AUTH_SALT', '$6ri@&t:0?pzbQ|<TqF7w$f?r;*Q#;sK=Gac>g:rrY>q+] ]BW/gW52*bR|.,;NE');
define('LOGGED_IN_SALT',   'w&,`~N2!+C?F*zLPuPh@[j0!mndG1RU3m%o+J}K1Zh/ 405AhE,^?(6$:Us@*kP1');
define('NONCE_SALT',       'fhINP!Rrr!rZ1mBp^<cL3;HSq*t(vLhLsWWs[b$m8H(8Tq.UGOK&IU`;$`uH8xWU');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
