<footer>
    <div class="row expanded collapse align-middle">
        <div class="columns">
            © 2014–2016  «АРТВИТИКО.Дом» Все права защищены.
        </div>
        <div class="columns text-right">
            <a href="mailto:denis@314zdato.ru" class="developer">Сайт сделан
                <svg version="1.1" baseProfile="tiny"
                     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24"
                     xml:space="preserve">
<path id="XMLID_3_" fill-rule="evenodd" fill="#B52421" d="M23.8,0.3v3.5h-5.9c0,0-0.6,12.9-0.7,13.8c0.4,1.4,0.8,1.9,2.6,2.6
	c2.9,0.3,3.5-3.2,3.5-3.2s0.3-0.7,0.5,0c-0.6,4.9-2.7,5.8-3.5,6.3c-0.8,0.5-3.6,0.3-4,0.1s-2.5-1.6-2.5-4.2c0-2.7,1.3-15.5,1.3-15.5
	l-6-0.1c0,0-0.4,11.4-1,13.6c-0.2,1.7,0.3,1.9-0.9,4.5s-3.7,1.9-3.7,1.9S1.1,22.4,2,19.8c1.7-1.9,1.8-2,2.4-2.9
	C5.8,15.1,7.6,3.6,7.6,3.6S5.5,3.3,4,3.5C2.7,3.6,0.8,7,0.6,7.2S0.1,7.3,0.1,6.9c0-0.4,1-3,1.6-3.9s2.5-2.3,3.9-2.4
	S23.8,0.3,23.8,0.3z"/>
</svg>
            </a>
        </div>
    </div>
</footer>
</main>
<?php foreach (get_posts(['post_type' => 'gallery']) as $key => $post): ?>
	<?php
		$slug = get_field('slug', $post->ID);
	?>
	<div class="reveal" id="gallery_full_<?php echo $slug ?>" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
	    <?php
	    $images = get_field('gallery', $post->ID);
	    if ($images):?>
	        <div id="gallery_modal_<?php echo $slug; ?>" class="owl-carousel owl-full">
	            <?php foreach ($images as $img): ?>
	                <div class="slide-item">
	                    <div class="img" style="background-image: url(<?php echo $img['url']; ?>);"></div>
	                </div>
	            <?php endforeach; ?>
	        </div>
	    <?php endif; ?>
	</div>
<?php endforeach; ?>

<div class="reveal" id="callback_form" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
    <h3>Обратный звонок</h3>
    Наш специалист свяжется с вами в ближайшее время
    <?php echo do_shortcode( '[contact-form-7 id="204" html_class="form row" title="Callback"]' ); ?>
</div>
<?php wp_footer(); ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter41048104 = new Ya.Metrika({
                    id:41048104,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/41048104" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>