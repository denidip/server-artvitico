<?php get_header(); ?>
        <article class="header">
            <div class="row expanded collapse align-middle">
                <div class="column large-4">
                    <a href="#" class="logo-name">Артвитико Дом</a>
                    <div class="slogan">Нестандартная корпусная мебель</div>
                </div>
                <div class="column large-8 text-right">
                    <a class="button callback go-callback"></a>
                    <div class="contact">
                        <a href="#" class="phone">8 3452 28-52-51</a>
                        <a class="callback go-callback">Обратный звонок</a>
                    </div>
                </div>
            </div>
        </article>
        <article class="first-screen">
            <div id="hello" class="owl-carousel owl-full">
                <div class="slide-item">
                    <div class="img" style="background-image: url(http://stroykadoma.org/wp-content/uploads/2015/07/%D1%88%D1%82%D0%BE%D1%80%D1%8B-%D0%B4%D0%BB%D1%8F-%D0%BA%D1%83%D1%85%D0%BD%D0%B8-1.jpg)"></div>
                    <div class="row align-middle align-center">
                        <div class="column large-10 text-center">
                            <h1>Делаем не просто кухни!</h1>
                            <p>
                                Совмещая новейшие технологии и техническую эстетику, даже из стандартных решений
                                создаем эксклюзив.
                            </p>
                            <a href="#howitwork" class="button scrolle">Узнать больше</a>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="img" style="background-image: url(http://shkafy-kupe-voronezh.ru/wp-content/uploads/2012/09/shkafyi-na-zakaz-v-Voronezhe.jpg)"></div>
                    <div class="row align-middle align-center">
                        <div class="column large-10 text-center">
                            <h1>Делаем не просто шкафы!</h1>
                            <p>
                                Максимально расширяем пространство задуманного, не забывая, что он должен быть стильным,
                                функциональным и конечно же гармонично вписываться в Ваш интерьер.
                            </p>
                            <a href="#howitwork" class="button scrolle">Узнать больше</a>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="img" style="background-image: url(http://www.dommebeli.su/sites/default/files/imagecache/product_full/mebel/Cinderella%203_0.JPG)"></div>
                    <div class="row align-middle align-center">
                        <div class="column large-10 text-center">
                            <h1>Детство – пора мечтаний и сказок,</h1>
                            <p>
                                C очаровательными феями, суперменами и фантастическими героями. Создавая детскую мебель, делаем ее не только интересной, но и главное комфортной, уютной и безопастной.
                            </p>
                            <a href="#howitwork" class="button scrolle">Узнать больше</a>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="img" style="background-image: url(http://www.imperialwood.ru/upload/iblock/07b/07b71b4285ada238da10dbbbeb0b04e8.jpg)"></div>
                    <div class="row align-middle align-center">
                        <div class="column large-10 text-center">
                            <h1>Торговое оборудование.</h1>
                            <p>
                                Современные технологии и материалы помогают воплотить в жизнь самые смелые и нестандартные идеи. Границ практически нет, все определяет желание и идея.
                            </p>
                            <a href="#howitwork" class="button scrolle">Узнать больше</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-tabs">
                <a href="#howitwork" id="kitchenMark" class="item scrolle active" data-slide="1">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/kitchen.png">
                    <div class="label devider">Кухни</div>
                </a>
                <a href="#howitwork" id="boxMark" class="item scrolle" data-slide="2">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/box.png">
                    <div class="label devider">Шкафы купе</div>
                </a>
                <a href="#howitwork" id="childroomMark" class="item scrolle" data-slide="3">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/childrenroom.png">
                    <div class="label devider">Детские</div>
                </a>
                <a href="#howitwork" id="childroomMark" class="item scrolle" data-slide="4">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/industrial.png">
                    <div class="label devider">Для бизнеса</div>
                </a>
            </div>
        </article>
        <article class="header white">
            <div class="row expanded collapse align-middle">
                <div class="column large-3">
                    <a href="#" class="logo-name">Артвитико Дом</a>
                    <div class="slogan">Нестандартная корпусная мебель</div>
                </div>
                <div class="column large-6 text-center">
                    <nav>
                        <a href="#howitwork" class="scrolle">О нас</a>
                        <a href="#folio" class="scrolle">Наши работы</a>
                        <a href="#manufacture" class="scrolle">Производство</a>
                        <a href="#map" class="scrolle">Контакты</a>
                    </nav>
                </div>
                <div class="column large-3 text-right">
                    <a class="button callback go-callback"></a>
                    <div class="contact">
                        <a href="#" class="phone">8 3452 28-52-51</a>
                        <a class="callback go-callback">Обратный звонок</a>
                    </div>
                </div>
            </div>
        </article>
        <article id="howitwork" class="about content">
            <div class="row">
                <div class="column large-12">
                    <h2 class="devider">Как мы работаем</h2>
                    <p>
                        С самого начала своего существования компания «Арт-Витико-Дом» специализируется на изготовлении мебели на заказ по индивидуальным проектам. Это своего рода выглядит как - мебельное ателье, но в рамках производственного предприятия. Производимая мебель нестандартного класса, конкретно под заказчика, исполняется как для общественных, так и для жилых помещений, зачастую по ценам серийных решений.
                    </p>
                </div>
            </div>
            <div class="row text-center steps">
                <div class="column large-3">
                    <div class="container">
                        <div class="count">1.</div>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/step_1.png">
                        <div class="handler">Оформление заявки</div>
                        С Вами связывается наш менеджер и ответит на все ваши вопросы.
                        <a class="button go-callback">Оформить</a>
                    </div>
                </div>
                <div class="column large-3">
                    <div class="container">
                        <div class="count">2.</div>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/step_2.png">
                        <div class="handler">Вызов замерщика</div>
                        Замерщик произведет профессиональный замер помещения с учетом всех пожеланий.
                    </div>
                </div>
                <div class="column large-3">
                    <div class="container">
                        <div class="count">3.</div>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/step_3.png">
                        <div class="handler">Изготовление</div>
                        После согласования проекта мебели заключается договор, в котором оговариваются все условия исполнения заказа и детали.
                    </div>
                </div>
                <div class="column large-3">
                    <div class="container">
                        <div class="count">4.</div>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/step_4_kitchen.png">
                        <div class="handler">Доставка и монтаж</div>
                        Доставка мебели к Вам происходит собственным транспортом компании в удобное для Вас время. Установка осуществляется опытными специалистами.
                    </div>
                </div>
            </div>
        </article>
        <article id="folio" class="content">
            <h2 class="devider">Наши работы</h2>
	        <?php
	            $posts = get_posts([
	            	'post_type' => 'gallery',
                    'order' => 'ASC'
	            ]);
	        ?>
            <ul class="tabs" data-tabs id="gallery-tabs">
	            <?php foreach ($posts as $key => $post): ?>
		            <?php
		                $slug = get_field('slug', $post->ID);
		            ?>
                    <li class="tabs-title <?php echo $key == 0 ? 'is-active' : '' ?>"><a href="#panel-<?php echo $slug ?>" aria-selected="true"><?php echo $post->post_title; ?></a></li>
				<?php endforeach; ?>
            </ul>
            <div class="tabs-content" data-tabs-content="gallery-tabs">
	            <?php foreach ($posts as $key => $post): ?>
		            <?php
		                $slug = get_field('slug', $post->ID);
		            ?>
	                <div class="tabs-panel <?php echo $key == 0 ? 'is-active' : '' ?>" id="panel-<?php echo $slug; ?>">
	                    <?php
	                    $images = get_field('gallery', $post->ID);
	                    if ($images):?>
	                        <div id="gallery-thumb-<?php echo $slug; ?>" class="owl-carousel owl-thumb">
	                            <?php foreach ($images as $key => $img): ?>
	                                <div class="slide-item" data-key="<?php echo $key ?>">
	                                    <div class="img" style="background-image: url(<?php echo $img['sizes']['medium_large']; ?>);"></div>
	                                </div>
	                            <?php endforeach; ?>
	                        </div>
	                    <?php endif; ?>
	                </div>
				<?php endforeach; ?>
            </div>
        </article>
        <article id="partners" class="content">
            <div class="row">
                <div class="column large-12">
                    <h2 class="devider">Качественные материалы и фурнитура</h2>
                </div>
            </div>
            <div class="row align-middle center-partners">
                <div class="column large-4 pictures">
                    <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/blum_1.jpg);"></div>
                    <div class="logo-partners" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/blum.png);"></div>
                    <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/blum_2.jpg);"></div>
                    <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/blum_3.jpg);"></div>
                </div>
                <div class="column large-8 text">
                    <h4>Фурнитура «Blum» для кухни позволяет:</h4>
                    <p>Cоздавать конструкции повышенной прочности и функциональности, превращать «мертвые» зоны в полезные, реализовывать любые дизайнерские задумки.</p>
                    <p>На практике даже «простейшая» кухонная фурнитура «Blum» делает зону приготовления пищи компактной, эргономичной, мультифункциональной.</p>
                    <p>Каждый, кто имеет дело с изделиями производства Blum, испытывает только позитивные эмоции.</p>
                </div>
            </div>
            <div class="row align-middle center-partners">
                <div class="column large-8 text text-right">
                    <h4>Продукция компании «GRATIS»</h4>
                    <p>Cодержит в себе весь необходимый перечень мебельной фурнитуры:
                    Аксессуары для кухни, сушилки для посуды, выдвижные корзины, выдвижные системы с доводчиком и без, рейлинговые системы, барные стойки и прочие.</p>
                    <p>Системы алюминевых профилей для шкафов-купе, системы наполнения шкафов, полки, различные виды корзин для вещей и обуви, вешало, держатели для брюк, галстуков и т.д.</p>
                </div>
                <div class="column large-4 pictures pic-right">
                    <div class="logo-partners" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/gratis.png);"></div>
                    <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/gratis_1.jpg);"></div>
                    <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/gratis_2.jpg);"></div>
                    <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/gratis_3.jpg);"></div>
                </div>
            </div>
            <div class="row">
                <?php
                $logos = get_field('logo', 2);
                if( $logos ): ?>
                    <div id="partners_gallery" class="owl-carousel owl-partners">
                        <?php foreach( $logos as $logo ): ?>
                            <div class="partner_card">
                                <img src="<?php echo $logo['url']; ?>">
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </article>
        <article id="manufacture" class="content video_overlay">
            <video id="video" width="100%" height="auto" autoplay="autoplay" loop="loop" preload="auto">
                <source src="<?php echo get_template_directory_uri(); ?>/video/manufacture.mp4"></source>
            </video>
            <div class="content-wrap text-center">
                <div class="row align-middle">
                    <div class="columns">
                        <h2>Собственное производство</h2>
                        <p>
                            На качество выпускаемой продукции мебели существенным критерием является производственное
                            оборудование, а именно его уровень или как говорят «класс» инструмента, станков. Наша
                            производственная база состоит из высокотехнологичного оборудования итало-германских
                            передовых компаний с мировым именем, таких как «SCM Grop» и «Festool». Данное оборудование
                            позволяет осуществлять высокоточную обработку и изготовление деталей.
                        </p>
                        <a class="button go-callback">Оставить заявку</a>
                    </div>
                </div>
            </div>
        </article>
        <article id="map" class="map">
            <div id="gis"></div>
            <div class="contacts">
                <div class="container">
                    <h4>Контакты</h4>
                    <ul>
                        <li class="i_mark">
                            Наш адрес:
                            <a href="#">
                                625000, г.Тюмень, ул. Константина Заслонова 37, стр. 45 «Б»
                            </a>
                        </li>
                        <li class="i_phone">
                            Телефон:
                            <a href="tel:83452285251">
                                8 3452 28-52-51
                            </a>
                        </li>
                        <li class="i_mail">
                            E-mail:
                            <a href="mailto:info@artvitico.ru">
                                info@artvitico.ru
                            </a>
                        </li>
                    </ul>
                    Мы в социальных сетях:
                    <div class="social">
                        <a href="https://vk.com/artvitico" class="vk"></a>
                        <!--<a href="#" class="tw"></a>
                        <a href="#" class="fb"></a>-->
                    </div>
                </div>
            </div>
        </article>
<?php get_footer(); ?>