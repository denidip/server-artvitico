var gulp = require('gulp');
var changed = require('gulp-changed');
var shell = require('gulp-shell');
var clean = require('gulp-clean');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var rename = require("gulp-rename");
var minifycss = require('gulp-minify-css');
var jsmin = require('gulp-jsmin');
var image = require('gulp-image');
var autoprefixer = require('gulp-autoprefixer');
var fileinclude = require('gulp-file-include');
var watch = require('gulp-watch');
var $ = require('gulp-load-plugins')();

var sassPaths = [
    'node_modules/foundation-sites/scss',
    'bower_components/animate.css/',
    'node_modules/owl.carousel/src/scss/owl.carousel.scss',
    'node_modules/motion-ui/src'
];

gulp.task('scss', function() {
        gulp.src('./css/**/*', {read: false})
            .pipe(clean());
        gulp.src('./assets/scss/app.scss')
            .pipe(sass({
                includePaths: sassPaths
            }).on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 2 versions'],
                cascade: false
            }))
            .pipe(minifycss())
            .pipe(rename({suffix: '.min'}))
            .pipe(changed('css'))
            .pipe(gulp.dest('./css'));
        gulp.src('./assets/scss/main.scss')
            .pipe(sass({
                includePaths: sassPaths
            }))
            .pipe(autoprefixer({
                browsers: ['last 2 versions'],
                cascade: false
            }))
            .pipe(sass())
            .pipe(minifycss())
            .pipe(rename({suffix: '.min'}))
            .pipe(changed('css'))
            .pipe(gulp.dest('./css'));
});
gulp.task('scripts-app', function(){
        gulp.src([
                './node_modules/foundation-sites/js/foundation.core.js',
                './node_modules/foundation-sites/js/foundation.reveal.js',
                './node_modules/foundation-sites/js/foundation.tabs.js',
                './node_modules/foundation-sites/js/foundation.util.keyboard.js',
                './node_modules/foundation-sites/js/foundation.util.mediaQuery.js',
                './node_modules/foundation-sites/js/foundation.util.box.js',
                './node_modules/foundation-sites/js/foundation.util.triggers.js',
                './node_modules/foundation-sites/js/foundation.util.motion.js',
                './node_modules/foundation-sites/js/foundation.util.timerAndImageLoader.js',
                './node_modules/owl.carousel/dist/owl.carousel.js'
            ])
            .pipe(changed('js'))
            .pipe(concat('app.js'))
            .pipe(jsmin())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest('./js'));

        gulp.src([
                './node_modules/jquery/dist/jquery.js'
            ])
            .pipe(changed('build/js'))
            .pipe(concat('jquery.js'))
            .pipe(jsmin())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest('./js'));

});

gulp.task('scripts-custom', function(){
    gulp.src('./assets/scripts/*.js')
        .pipe(changed('js'))
        .pipe(concat('main.js'))
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./js'));
});

gulp.task('images', function(){
        gulp.src('./assets/images/**/*')
            .pipe(changed('img'))
            .pipe(image({
                pngquant: true,
                optipng: false,
                zopflipng: true,
                advpng: true,
                jpegRecompress: false,
                jpegoptim: true,
                mozjpeg: true,
                gifsicle: true,
                svgo: true
            }))
            .pipe(gulp.dest('./img/'));
});
gulp.task('video', function() {
    gulp.src('./video/*', {read: false})
        .pipe(clean());
    gulp.src('./assets/video/*')
        .pipe(gulp.dest('./video/'));
});

gulp.task('kill', shell.task([
    'killall -9 gulp',
    'echo Gulp killed!'
]));

gulp.task('default',['scss','scripts-app','scripts-custom','images','video'], function() {
    watch('./assets/scss/**/*.scss', function() {gulp.start('scss');});
    watch('./assets/scripts/*.js', function() {gulp.start('scripts-custom');});
    watch('./assets/images/**/*', function() {gulp.start('images');});
    watch('./assets/video/**/*', function() {gulp.start('video');});
});






