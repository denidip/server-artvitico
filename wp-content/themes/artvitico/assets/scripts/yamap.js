$(document).ready(function(){
    var map;

    DG.then(function () {
        map = DG.map('gis', {
            center: [57.160515, 65.650292],
            zoom: 16,
            fullscreenControl: false,
            zoomControl: false,
            scrollWheelZoom: false
        });
        DG.marker([57.160485652073, 65.6515689791]).addTo(map).bindPopup('Мы всегда рады гостям :)');
    });

});

