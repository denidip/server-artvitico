
$(document).ready(function(){

    galleryImg = $('.owl-thumb .slide-item');

    function SyncHeight() {
        $(".owl-thumb").css('height', galleryImg.width());

        console.log('WORK: '+ galleryImg.width());
    }
    var fullSlider = $("#hello");

    fullSlider.owlCarousel({
        items: 1,
        singleItem: true,
        dots: true,
        loop: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        smartSpeed: 700
    });

    var galleryThumb = $(".owl-thumb");

    galleryThumb.owlCarousel({
        items: 4,
        nav: true,
        dots: false,
        loop: true,
        onInitialized: SyncHeight,
        onResize: SyncHeight
    });

    var partners = $("#partners_gallery");

    partners.owlCarousel({
        items: 5,
        nav: false,
        dots: false,
        loop: true,
        autoplay: true,
        autoplayTimeout: 3000,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        smartSpeed: 700
    });

    var galleries = $(".owl-full");

    $.each(galleries, function(key, gallery) {
      $(gallery).owlCarousel({
        items: 1,
        singleItem: true,
        nav: true,
        dots: false,
        loop: true,
        smartSpeed: 700
      });
    });

    $(".owl-thumb .slide-item").on('click', function(){
      var gallerySlug = $(this).parents('.tabs-panel').attr('id').split('-')[1];
      var slider = $('#gallery_modal_' + gallerySlug);

      $('#gallery_full_' + gallerySlug).foundation('open');

      var key = $(this).attr('data-key');

      slider.data('owl.carousel').to(key)
    });

    var sliderTabs = $('.slider-tabs .item');

    sliderTabs.on('mouseenter', function(){
        sliderTabs.removeClass('active');
        $(this).addClass('active');
        var currentSlideNumber = $(this).data('slide');
        $(".owl-carousel").trigger('to.owl.carousel', currentSlideNumber - 1);
        console.log(currentSlideNumber);
    });

    $(window).scroll(function() {
        if ($(window).scrollTop() > 20){
            $('.logo').addClass('scroll');
        }else{
            $('.logo').removeClass('scroll');
        }
        if ($(window).scrollTop() > $(window).height() - 86){
            $('.logo').addClass('inverse');
        }else{
            $('.logo').removeClass('inverse');
        }
        if ($(window).scrollTop() > $(window).height()){
            $('.header.white').addClass('scroll');
            $('main').addClass('scroll');
        }else{
            $('.header.white').removeClass('scroll');
            $('main').removeClass('scroll');
        }
    });

    $('a[href^="#"].scrolle, a[href^="."]').click( function(){ // если в href начинается с # или ., то ловим клик
        console.log('CLICK!!!!');
        var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href
        if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
            $('html, body').animate({ scrollTop: $(scroll_el).offset().top - 100 }, 1200); // анимируем скроолинг к элементу scroll_el
        }
        return false; // выключаем стандартное действие
    });

    $('.go-callback').click(function(){
        $('#callback_form').foundation('open');
    });
    $('input#phone_number').mask("+7(999) 999 99 99");
    $('input#phone_number').focusout(function(){
        if($(this).val().indexOf("_") +1 > 0){
            $(this).val('');
        };
    });

    $(document).foundation();
});