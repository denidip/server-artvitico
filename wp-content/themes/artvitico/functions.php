<?php

add_filter('show_admin_bar', '__return_false');

function add_theme_scripts() {
    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false );
    wp_enqueue_script( 'app', get_template_directory_uri() . '/js/app.min.js', false );
    wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.min.js', false );
    wp_enqueue_script( '2gis', 'http://maps.api.2gis.ru/2.0/loader.js?pkg=full' );
	//wp_enqueue_script( 'map', get_template_directory_uri() . '/assets/scripts/yamap.js', false );

    wp_register_style('gf-PT-serif', 'https://fonts.googleapis.com/css?family=PT+Serif:700&amp;subset=cyrillic');
    wp_enqueue_style( 'gf-PT-serif');

    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );



function get_gallery(){
    $posts = get_posts(array(
        'post_type'			=> 'gallery',
        'page_id' => $_GET['gallery_id']
    ));
    $images = array();
    foreach( $posts as $post ) {
        $fields = get_field('photo', $post->ID);
        foreach ($fields as $field) {
            $images[] = $field['url'];
        }

    }
    echo(json_encode( array('status'=>'ok', 'images'=>$images) ));
    wp_die();
}
add_action( 'wp_ajax_get_gallery',        'get_gallery' ); // For logged in users
add_action( 'wp_ajax_nopriv_get_gallery', 'get_gallery' ); // For anonymous users


function js_variables(){
    $variables = array (
        'ajax_url' => admin_url('admin-ajax.php'),
        'is_mobile' => wp_is_mobile()
        // Тут обычно какие-то другие переменные
    );
    echo('<script type="text/javascript">window.wp_data = ' . json_encode($variables) . '</script>');
}

add_action('wp_head','js_variables');